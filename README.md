# Time Zones problem statement

1. API Users must be able to create an account and log in.

2. All API calls must be authenticated.

3. When logged in, a user can see, edit and delete timezones he entered.

4. Implement 2 roles with different permission levels: a regular user would only be able to CRUD on their owned records, and an admin would be able to CRUD all users and all user records.

5. When a timezone is entered, each entry has a Name, Name of the city in timezone, the difference to GMT time.

6. The API must be able to return data in the JSON format.

7. Write unit tests.

package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/drmule100/time-zones/handler"
	"gitlab.com/drmule100/time-zones/model"
	"gitlab.com/drmule100/time-zones/service"
)

func main() {
	app := fiber.New()

	//allow authntication from user
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	//connect to db
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	dbCtx, err := model.New(ctx)
	if err != nil {
		fmt.Printf("error while initializing dbContext: %v", err)
	}
	//fmt.Println(client.Ping(ctx, nil)) //to test db connection

	timezoneSvc := service.New(dbCtx)
	logonSvc := service.NewLogonService(dbCtx)

	//todo
	logonHandler := handler.RegisterContext{
		LogonSvc: logonSvc,
	}

	logonHandler.RegisterHandlers(app)

	timezoneHandler := handler.TimezoneContext{
		TimezoneSvc: timezoneSvc,
	}
	timezoneHandler.RegisterHandlers(app)

	userSvc := service.NewUserSvc(dbCtx)
	userHandler := handler.UserContext{
		UserSvc: userSvc,
	}

	userHandler.RegisterHandlers(app)
	/* app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	}) */

	log.Println("setting up....")
	app.Listen(":3000")
}

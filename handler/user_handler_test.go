package handler_test

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"gitlab.com/drmule100/time-zones/handler"
	mock "gitlab.com/drmule100/time-zones/model/Mock"
	"gitlab.com/drmule100/time-zones/service"
)

func Test_GetUser(t *testing.T) {

	tests := []test{
		{
			Name:         "success",
			ReqBody:      "user2",
			StatusCode:   200,
			ExpectedResp: `{"name":"User2","userId":"user2","emailId":"user2@email.com"}`,
			User:         "user2",
		},
		{
			Name:         "Bad request",
			ReqBody:      "notUser",
			StatusCode:   400,
			ExpectedResp: `{"message":"mongo: no documents in result"}`,
			User:         "admin",
		},
		{
			Name:         "internal server error",
			ReqBody:      "user2-DbError",
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
			User:         "admin",
		},
		{
			Name:         "different user Get operation",
			ReqBody:      "user1",
			StatusCode:   401,
			ExpectedResp: `{"message":"you are not authorized for this request, only admin user or perticular user can access users record"}`,
			User:         "user2",
		},
	}

	req := fasthttp.Request{}
	req.Header.SetMethod("GET")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)
	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	userCtx := handler.UserContext{
		UserSvc: service.NewUserSvc(&mockDb),
	}

	userCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	userCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		token := login(ts.User, "user2@100")
		c.Request().Header.SetCookie("token", token)
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		reqCtx.Request.SetRequestURI("/api/v1/user/" + ts.ReqBody)
		err := userCtx.GetUser(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_GetUsers(t *testing.T) {

	tests := []test{
		{
			Name:         "success",
			ReqBody:      "user2",
			StatusCode:   200,
			ExpectedResp: `[{"name":"User2","userId":"user2","emailId":"user2@email.com"}]`,
			User:         "admin",
		},
		{
			Name:         "Unauthorized user",
			ReqBody:      "notUser",
			StatusCode:   401,
			ExpectedResp: `{"message":"you are not authorized for this request, only admin user can access users record"}`,
			User:         "user2",
		},
	}

	req := fasthttp.Request{}

	req.Header.SetMethod("GET")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	userCtx := handler.UserContext{
		UserSvc: service.NewUserSvc(&mockDb),
	}

	userCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	userCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		token := login(ts.User, "user2@100")
		c.Request().Header.SetCookie("token", token)
		reqCtx.Request.SetRequestURI("/api/v1/users")
		err := userCtx.GetUsers(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_UpdateUser(t *testing.T) {

	badReq := `{"name":"User3","userId":"user3","emailId":"user2@email.com"}`
	serverErrorReq := `{"name":"badUser","userId":"user2","emailId":"user2@email.com"}`
	validReq := `{"name":"User3","userId":"user2","emailId":"user2@email.com"}`
	tests := []test{
		{
			Name:         "name change success",
			ReqBody:      validReq,
			StatusCode:   200,
			ExpectedResp: `{"name":"User3","userId":"user2","emailId":"user2@email.com"}`,
			Param:        "user2",
		},
		{
			Name:         "Bad request",
			ReqBody:      badReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"userId and emailId can not be changed"}`,
			Param:        "user2",
		},
		{
			Name:         "internal server error",
			ReqBody:      serverErrorReq,
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
			Param:        "user2",
		},
	}

	req := fasthttp.Request{}

	req.Header.SetMethod("PUT")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	userCtx := handler.UserContext{
		UserSvc: service.NewUserSvc(&mockDb),
	}

	userCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	token := login("user2", "user2@100")
	c.Request().Header.SetCookie("token", token)

	userCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		reqCtx.Request.SetRequestURI("/api/v1/users/" + ts.Param)
		err := userCtx.UpdateUser(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_DeleteUser(t *testing.T) {

	tests := []test{
		{
			Name:         "user deleted successfully",
			StatusCode:   204,
			ExpectedResp: `{"message":"user deleted successfully"}`,
			Param:        "user3",
			User:         "user3",
		},
		{
			Name:         "User not present",
			StatusCode:   400,
			ExpectedResp: `{"message":"mongo: no documents in result"}`,
			Param:        "notUser",
			User:         "admin",
		},
		{
			Name:         "internal server error",
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
			Param:        "userError",
			User:         "admin",
		},
	}

	req := fasthttp.Request{}

	req.Header.SetMethod("DELETE")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	userCtx := handler.UserContext{
		UserSvc: service.NewUserSvc(&mockDb),
	}
	userCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)

	userCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		//reqCtx.Request.SetBody([]byte(ts.ReqBody))
		token := login(ts.User, "user2@100")
		c.Request().Header.SetCookie("token", token)
		reqCtx.Request.SetRequestURI("/api/v1/users/" + ts.Param)
		err := userCtx.DeleteUser(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_PromoteUser(t *testing.T) {
	tests := []test{
		{
			Name:         "user promoted successfully",
			StatusCode:   200,
			ExpectedResp: `{"message":"user promoted successfully"}`,
			Param:        "user3",
			User:         "admin",
		},
		{
			Name:         "User not present",
			StatusCode:   400,
			ExpectedResp: `{"message":"mongo: no documents in result"}`,
			Param:        "notUser",
			User:         "admin",
		},
		{
			Name:         "internal server error",
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
			Param:        "userError",
			User:         "admin",
		},
	}
	req := fasthttp.Request{}
	req.Header.SetMethod("PUT")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	userCtx := handler.UserContext{
		UserSvc: service.NewUserSvc(&mockDb),
	}

	userCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)

	userCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		token := login(ts.User, "user2@100")
		c.Request().Header.SetCookie("token", token)
		reqCtx.Request.SetRequestURI("/api/v1/users/promote/" + ts.Param)
		err := userCtx.PromoteUser(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

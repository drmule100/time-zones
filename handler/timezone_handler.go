package handler

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/drmule100/time-zones/model"
	"gitlab.com/drmule100/time-zones/service"
	"go.mongodb.org/mongo-driver/mongo"
)

type TimezoneContext struct {
	TimezoneSvc *service.TimezoneSvc
}

func (h *TimezoneContext) RegisterHandlers(app *fiber.App) {
	app.Post("/api/v1/timezone", h.CreateTimeZones)
	app.Get("/api/v1/timezones", h.GetTimeZones)
	app.Get("/api/v1/timezones/:id", h.GetTimeZone)
	app.Put("/api/v1/timezones/:id", h.UpdateTimeZone)
	app.Delete("/api/v1/timezones/:id", h.DeleteTimeZone)
	app.Get("/api/v1/time/:id", h.GetTime)
}

func (h *TimezoneContext) CreateTimeZones(c *fiber.Ctx) error {
	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	userId := claims.Issuer

	var timezone model.TimeZone
	err = c.BodyParser(&timezone)
	if err != nil {
		fmt.Printf("c.BodyParser() failed with %v", err)
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "provided request body is incorrect",
		})
	}
	timezone.User = userId

	tz, statusCode, err := h.TimezoneSvc.CreateTimeZone(c.UserContext(), &timezone)
	if err != nil {
		c.Status(statusCode)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	c.Status(fiber.StatusCreated)
	return c.JSON(tz)
}

func (h *TimezoneContext) GetTimeZones(c *fiber.Ctx) error {
	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	tzs, err := h.TimezoneSvc.GetTimeZones(c.UserContext(), claims.Issuer)
	if err != nil {
		if err != mongo.ErrNoDocuments {
			c.Status(fiber.StatusInternalServerError)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
	}

	return c.JSON(tzs)
}

func (h *TimezoneContext) UpdateTimeZone(c *fiber.Ctx) error {
	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	var timezone model.TimeZone
	err = c.BodyParser(&timezone)
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "provided request body is incorrect",
		})
	}

	//id := c.Params("id")
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	id := pathParts[len(pathParts)-1]

	if timezone.User != "" {
		if claims.Issuer != timezone.User { //todo check if admin
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": fmt.Sprintf("you are not allowed to modify %s timezone", id),
			})
		}
	} else {
		timezone.User = claims.Issuer
	}

	tz, err := h.TimezoneSvc.UpdateTimeZone(c.Context(), claims.Issuer, id, timezone)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	return c.JSON(tz)
}

func (h *TimezoneContext) DeleteTimeZone(c *fiber.Ctx) error {

	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	//id := c.Params("id")  //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	id := pathParts[len(pathParts)-1]

	statusCode, err := h.TimezoneSvc.DeleteTimeZone(c.UserContext(), claims.Issuer, id)
	if err != nil {
		c.Status(statusCode)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	c.Status(fiber.StatusNoContent)

	return c.JSON(fiber.Map{
		"message": fmt.Sprintf("time zone with id %s deleted successfully", id),
	})

}

func (h *TimezoneContext) GetTimeZone(c *fiber.Ctx) error {

	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	//id := c.Params("id")  //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	id := pathParts[len(pathParts)-1]

	tz, statusCode, err := h.TimezoneSvc.GetTimezone(c.UserContext(), claims.Issuer, id)
	if err != nil {
		c.Status(statusCode)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	c.Status(statusCode)

	return c.JSON(tz)

}

func (h *TimezoneContext) GetTime(c *fiber.Ctx) error {
	claims, err := IsAuthorize(c)
	if err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}

	//id := c.Params("id")  //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	id := pathParts[len(pathParts)-1]

	t, statusCode, err := h.TimezoneSvc.GetTime(c.UserContext(), claims.Issuer, id)

	if err != nil {
		c.Status(statusCode)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	c.Status(statusCode)
	return c.JSON(fiber.Map{
		"time": t,
	})
}

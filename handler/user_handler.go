package handler

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/drmule100/time-zones/model"
	"gitlab.com/drmule100/time-zones/service"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

type UserContext struct {
	UserSvc *service.UserSvc
}

func (h *UserContext) RegisterHandlers(app *fiber.App) {
	//app.Post("/api/v1/user", h.CreateUser)
	app.Get("/api/v1/users", h.GetUsers)
	app.Get("/api/v1/users/:userId", h.GetUser)
	app.Put("/api/v1/users/:userId", h.UpdateUser)
	app.Put("/api/v1/users/promote/:userId", h.PromoteUser)
	app.Delete("/api/v1/users/:userId", h.DeleteUser)
}

func (h *UserContext) GetUsers(c *fiber.Ctx) error {
	op := "Get users"
	claims, err := IsAuthorize(c)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}
	userId := claims.Issuer
	isAdmin := h.UserSvc.IsAdminUser(c.UserContext(), userId)

	// only admin can get all the record
	if !isAdmin {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "you are not authorized for this request, only admin user can access users record",
		})
	}

	users, err := h.UserSvc.GetAllUser(c.UserContext())

	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	fmt.Printf("operation %s completed successfully \n", op)
	c.Status(fiber.StatusOK)
	return c.JSON(users)

}

// UpdateUser: update the requested user with given data
func (h *UserContext) UpdateUser(c *fiber.Ctx) error {
	op := "Update user"
	claims, err := IsAuthorize(c)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}
	//requestedUserId := c.Params("userId") //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	requestedUserId := pathParts[len(pathParts)-1]
	userId := claims.Issuer

	user, err := h.UserSvc.GetUser(c.UserContext(), requestedUserId)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	isAdmin := h.UserSvc.IsAdminUser(c.UserContext(), userId)
	// only admin or perticular user can modify the record
	if !isAdmin && userId != requestedUserId {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "you are not authorized for this request, only admin user can access users record",
		})
	}

	var req model.User
	err = c.BodyParser(&req)
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	if req.Password != "" {
		pwd, err := bcrypt.GenerateFromPassword([]byte(req.Password), 14)
		if err != nil {
			return err
		}

		user.CryptedPassword = pwd
	}

	if req.Name != "" {
		user.Name = req.Name
	}

	if req.EmailID != user.EmailID || req.UserID != user.UserID {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "userId and emailId can not be changed",
		})
	}

	updatedUser, err := h.UserSvc.UpdateUser(c.UserContext(), user)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	fmt.Printf("operation %s completed successfully \n", op)
	c.Status(fiber.StatusOK)
	return c.JSON(updatedUser)
}

// GetUsers will fetch user from db and return
func (h *UserContext) GetUser(c *fiber.Ctx) error {
	op := "Get user"
	claims, err := IsAuthorize(c)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}
	//requestedUserId := c.Params("userId")
	//todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	requestedUserId := pathParts[len(pathParts)-1]
	userId := claims.Issuer
	isAdmin := h.UserSvc.IsAdminUser(c.UserContext(), userId)

	// only admin or perticular user can get the record
	if !isAdmin && userId != requestedUserId {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "you are not authorized for this request, only admin user or perticular user can access users record",
		})
	}

	user, err := h.UserSvc.GetUser(c.UserContext(), requestedUserId)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	fmt.Printf("operation %s completed successfully \n", op)
	c.Status(fiber.StatusOK)
	return c.JSON(user)

}

// DeleteUser deletes the user record from db
func (h *UserContext) DeleteUser(c *fiber.Ctx) error {
	op := "Delete user"
	claims, err := IsAuthorize(c)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}
	//requestedUserId := c.Params("userId") //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	requestedUserId := pathParts[len(pathParts)-1]
	userId := claims.Issuer
	isAdmin := h.UserSvc.IsAdminUser(c.UserContext(), userId)

	// only admin or perticular user can get the record
	if !isAdmin && userId != requestedUserId {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "you are not authorized for this request, only admin OR perticular user can delete users record",
		})
	}

	err = h.UserSvc.DeleteUser(c.UserContext(), requestedUserId)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}
	fmt.Printf("operation %s completed successfully \n", op)
	c.Status(fiber.StatusNoContent)
	return c.JSON(fiber.Map{
		"message": "user deleted successfully",
	})

}

func (h *UserContext) PromoteUser(c *fiber.Ctx) error {
	op := "promote user"
	claims, err := IsAuthorize(c)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthorized user",
		})
	}
	//requestedUserId := c.Params("userId") //todo for testing
	uri := string(c.Context().RequestURI())
	pathParts := strings.Split(uri, "/")
	requestedUserId := pathParts[len(pathParts)-1]
	userId := claims.Issuer
	isAdmin := h.UserSvc.IsAdminUser(c.UserContext(), userId)

	// only admin or perticular user can get the record
	if !isAdmin {
		fmt.Printf("%s is requested by non admin user- denied", op)
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "you are not authorized for this request, only admin  can promote user",
		})
	}

	err = h.UserSvc.PromoteUser(c.UserContext(), requestedUserId)
	if err != nil {
		fmt.Printf("Error %s operation: %v", op, err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	fmt.Printf("operation %s completed successfully \n", op)
	c.Status(fiber.StatusOK)
	return c.JSON(fiber.Map{
		"message": "user promoted successfully",
	})

}

package handler

import (
	"fmt"
	"log"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"gitlab.com/drmule100/time-zones/model"
	"gitlab.com/drmule100/time-zones/service"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

const (
	UserDB         = "TimeZone"
	UserCollection = "user"
	SecretKey      = "topSecret" //todo read from env vars
)

type RegisterContext struct {
	//DbClient *mongo.Client
	LogonSvc *service.LogonSvc
}

func (h *RegisterContext) RegisterHandlers(app *fiber.App) {
	app.Post("/api/v1/register", h.RegisterUser)
	app.Post("/api/v1/login", h.LoginUser)
	app.Post("/api/v1/logout", h.Logout)
}

func (h *RegisterContext) RegisterUser(c *fiber.Ctx) error {

	//todo handle validations
	var user model.User
	err := c.BodyParser(&user)
	if err != nil {
		return err
	}

	if user.UserID == "" || user.Password == "" || user.EmailID == "" || user.Name == "" {
		log.Println("name/userId/password/emailId are missing in register request")
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "name, userId, password and emailId are mandatory to register",
		})
	}

	pwd, err := bcrypt.GenerateFromPassword([]byte(user.Password), 14)
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	user.CryptedPassword = pwd
	statusCode, err := h.LogonSvc.SaveUser(c.Context(), user)
	if err != nil {
		if statusCode != 0 {
			c.Status(statusCode)
		} else {
			c.Status(fiber.StatusInternalServerError)
		}
		return c.JSON(fiber.Map{
			"message": err.Error(),
		})
	}

	return c.Status(fiber.StatusCreated).JSON(
		fiber.Map{
			"message": "Registration successfully completed....!",
		})
}

func (h *RegisterContext) LoginUser(c *fiber.Ctx) error {

	var cred model.LoginCredientials
	err := c.BodyParser(&cred)
	if err != nil {
		return err
	}

	if cred.Password == "" || cred.UserID == "" {
		return c.Status(fiber.StatusBadRequest).JSON(
			fiber.Map{
				"message": "userId and password can not be empty or missing",
			})
	}

	user, err := h.LogonSvc.UserByID(c.Context(), cred.UserID)
	if err != nil {
		fmt.Printf("LogonSvc.UserByID() failed with %v", err)
		if err == mongo.ErrNoDocuments {
			c.Status(fiber.StatusBadRequest)
			return c.JSON(fiber.Map{
				"message": "User not found",
				//"additionalInfo": err.Error(),
			})
		}
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": "internal server error, please try after some time",
			//"additionalInfo": err.Error(),
		})

	}

	//password comparison
	err = bcrypt.CompareHashAndPassword(user.CryptedPassword, []byte(cred.Password))
	if err != nil {
		c.Status(fiber.StatusBadRequest)
		return c.JSON(fiber.Map{
			"message": "incorrect password",
		})
	}

	// generate token for user
	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    user.UserID,
		ExpiresAt: time.Now().Add(30 * time.Minute).Unix(), //expires in 30 minutes
	})

	token, err := claims.SignedString([]byte(SecretKey))
	if err != nil {
		c.Status(fiber.StatusInternalServerError)
		return c.JSON(fiber.Map{
			"message": "could not login",
		})
	}

	cookie := fiber.Cookie{
		Name:     "token",
		Value:    token,
		Expires:  time.Now().Add(3000 * time.Minute),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	return c.JSON(fiber.Map{
		"message": "login successfull",
	})
}

func (h *RegisterContext) Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "token",
		Value:    "",
		Expires:  time.Now(),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)
	//c.ClearCookie("token")
	return c.JSON(fiber.Map{
		"message": "successfully logout",
	})

}

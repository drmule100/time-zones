package handler_test

import (
	"fmt"
	"testing"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"gitlab.com/drmule100/time-zones/handler"
	mock "gitlab.com/drmule100/time-zones/model/Mock"
	"gitlab.com/drmule100/time-zones/service"
)

func Test_CreateTimezone(t *testing.T) {

	//login with valid user
	token := login("user2", "user2@100") //user and pwd are for testing purpose

	badReq := `{
		"name": "CT",
		"city":12,
		"GMT_Difference": "-6:00"
	}`
	/* duplicateReq := `
	{
		"name": "CT_exist",
		"city":"NY",
		"GMT_Difference": "-6:00"
	}` */
	serverErrorReq := `
	{
		"name": "new",
		"city":"dbError",
		"GMT_Difference": "-6:00"
	}`
	validReq := `
	{
		"name": "new",
		"city":"NY",
		"GMT_Difference": "-6:00"
	}`

	tests := []test{
		{
			Name:         "success",
			ReqBody:      validReq,
			StatusCode:   201,
			ExpectedResp: `{"_id":"000000000000000000000000","name":"new","city":"NY","GMT_Difference":"-6:00"}`,
		},
		{
			Name:         "Bad request",
			ReqBody:      badReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"provided request body is incorrect"}`,
		},
		/* {
			Name:         "duplicate create request",
			ReqBody:      duplicateReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"time zone with given name is already present, to update it use update/put api"}`,
		}, */
		{
			Name:         "internal server error",
			ReqBody:      serverErrorReq,
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
		},
	}

	req := fasthttp.Request{}
	req.SetRequestURI("/api/v1/timezone")
	req.Header.SetMethod("POST")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	c.Request().Header.SetCookie("token", token)

	mockDb := mock.MockDB{}
	tzCtx := handler.TimezoneContext{
		TimezoneSvc: service.New(&mockDb),
	}

	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		c.Context().SetUserValue("Name", ts.Name)
		err := tzCtx.CreateTimeZones(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_UpdateTimezone(t *testing.T) {
	badReq := `{
		"name": "IST1_Mumbai",
		"city": 12,
		"GMT_Difference": "+5:30"
	}`
	serverErrorReq := `
	{
		"name": "IST1_Mumbai",
		"city": "dbError",
		"GMT_Difference": "+5:30"
	}`
	validReq := `
	{
		"name": "IST1_Mumbai",
		"city": "Mumbai",
		"GMT_Difference": "+5:30",
		"_id":"6200c330b5e0854e6444ffee"
	}`
	tests := []test{
		{
			Name:         "success",
			ReqBody:      validReq,
			StatusCode:   200,
			ExpectedResp: `{"_id":"000000000000000000000000","name":"IST1","city":"","GMT_Difference":"-6:00"}`,
		},
		{
			Name:         "Bad request",
			ReqBody:      badReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"provided request body is incorrect"}`,
		},
		{
			Name:         "dbError",
			ReqBody:      serverErrorReq,
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
		},
	}

	req := fasthttp.Request{}
	req.SetRequestURI("/api/v1/timezone/6200c330b5e0854e6444ffee")
	req.Header.SetMethod("PUT")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	reqCtx.SetUserValue("Name", "name")
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	tzCtx := handler.TimezoneContext{
		TimezoneSvc: service.New(&mockDb),
	}

	tzCtx.RegisterHandlers(app)

	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)

	token := login("user2", "user2@100")
	c.Request().Header.SetCookie("token", token)

	tzCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		c.Context().SetUserValue("Name", ts.Name)
		err := tzCtx.UpdateTimeZone(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}
}

func Test_DeleteTimezone(t *testing.T) {
	tests := []test{
		{
			Name:         "success",
			ReqBody:      "000000000000000000000001",
			StatusCode:   204,
			ExpectedResp: `{"message":"time zone with id 000000000000000000000001 deleted successfully"}`,
		},
		{
			Name:         "Bad request",
			ReqBody:      "000000000000000000000002",
			StatusCode:   400,
			ExpectedResp: `{"message":"mongo: no documents in result"}`,
		},
		{
			Name:         "internal server error",
			ReqBody:      "000000000000000000000003",
			StatusCode:   500,
			ExpectedResp: `{"message":"error while fetching timezone: internal server error, please try after some time"}`,
		},
		{
			Name:         "different user delete operation",
			ReqBody:      "000000000000000000000004",
			StatusCode:   401,
			ExpectedResp: `{"message":"you are not allowed to delete ObjectID(\"000000000000000000000000\") timezone"}`,
		},
	}

	req := fasthttp.Request{}
	req.Header.SetMethod("DELETE")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	tzCtx := handler.TimezoneContext{
		TimezoneSvc: service.New(&mockDb),
	}

	tzCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	token := login("user2", "user2@100")
	c.Request().Header.SetCookie("token", token)

	tzCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		reqCtx.Request.SetRequestURI("/api/v1/timezone/" + ts.ReqBody)
		err := tzCtx.DeleteTimeZone(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_GetTimezone(t *testing.T) {

	tests := []test{
		{
			Name:         "success",
			ReqBody:      "000000000000000000000001",
			StatusCode:   200,
			ExpectedResp: `{"_id":"000000000000000000000000","name":"IST1","city":"","GMT_Difference":"-6:00"}`,
		},
		{
			Name:         "Bad request",
			ReqBody:      "000000000000000000000002",
			StatusCode:   400,
			ExpectedResp: `{"message":"mongo: no documents in result"}`,
		},
		{
			Name:         "internal server error",
			ReqBody:      "000000000000000000000003",
			StatusCode:   500,
			ExpectedResp: `{"message":"error while fetching timezone: internal server error, please try after some time"}`,
		},
		{
			Name:         "different user Get operation",
			ReqBody:      "000000000000000000000004",
			StatusCode:   401,
			ExpectedResp: `{"message":"you are not allowed to fetch 000000000000000000000004 timezone"}`,
		},
	}

	req := fasthttp.Request{}
	req.Header.SetMethod("GET")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)
	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	tzCtx := handler.TimezoneContext{
		TimezoneSvc: service.New(&mockDb),
	}

	tzCtx.RegisterHandlers(app)
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	token := login("user2", "user2@100")
	c.Request().Header.SetCookie("token", token)

	tzCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		reqCtx.Request.SetRequestURI("/api/v1/timezone/" + ts.ReqBody)
		err := tzCtx.GetTimeZone(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}

}

func Test_GetTimezones(t *testing.T) {

	tests := []test{
		{
			Name:         "success",
			ReqBody:      "000000000000000000000001",
			StatusCode:   200,
			ExpectedResp: `[{"_id":"000000000000000000000000","name":"IST1","city":"","GMT_Difference":"-6:00"},{"_id":"000000000000000000000000","name":"IST2","city":"","GMT_Difference":"-6:30"}]`,
			User:         "user2",
		},
		{
			Name:         "No timezone",
			ReqBody:      "000000000000000000000002",
			StatusCode:   200,
			ExpectedResp: `[]`,
			User:         "user2-new",
		},
		{
			Name:         "internal server error",
			ReqBody:      "000000000000000000000003",
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
			User:         "user2-DbError",
		},
	}

	req := fasthttp.Request{}

	req.Header.SetMethod("GET")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)
	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))
	mockDb := mock.MockDB{}
	tzCtx := handler.TimezoneContext{
		TimezoneSvc: service.New(&mockDb),
	}

	tzCtx.RegisterHandlers(app)

	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)

	tzCtx.RegisterHandlers(app)
	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		token := login(ts.User, "user2@100")
		c.Request().Header.SetCookie("token", token)
		err := tzCtx.GetTimeZones(c)
		assert.NoError(t, err, ts.Name+"Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+": expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+": response not matched")
	}
}

func login(userID, pwd string) string {

	claims := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.StandardClaims{
		Issuer:    userID,
		ExpiresAt: time.Now().Add(10 * time.Minute).Unix(), //expires in 10 minutes
	})
	token, err := claims.SignedString([]byte("topSecret"))
	if err != nil {
		fmt.Println("token extraction failed")
	}
	return token
}

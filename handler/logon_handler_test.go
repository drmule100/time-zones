package handler_test

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/stretchr/testify/assert"
	"github.com/valyala/fasthttp"
	"gitlab.com/drmule100/time-zones/handler"
	mock "gitlab.com/drmule100/time-zones/model/Mock"
	"gitlab.com/drmule100/time-zones/service"
)

type test struct {
	Name         string
	ReqBody      string
	StatusCode   int
	ExpectedResp string
	Param        string
	User         string
}

func Test_RegisterHandlers(t *testing.T) {

	validReq := `{
		"name": "user2",
		"userId": "user2",
		"emailId":"user2@gmail.com",
		"password":"user2@100"
	}`
	badReq := `{
		"name": "user2",
		"userId": "user2",
		"emailId":"user2@gmail.com"
	}`
	duplicateReq := `{
		"name": "user2",
		"userId": "xxx_Already_Exist_XXX",
		"emailId":"user2@gmail.com",
		"password":"user2@100"
	}`
	dbCrashReq := `{
		"name": "user2",
		"userId": "dbError",
		"emailId":"user2@gmail.com",
		"password":"user2@100"
	}`
	app := fiber.New()

	type test struct {
		Name         string
		ReqBody      string
		StatusCode   int
		ExpectedResp string
	}

	tests := []test{
		{
			Name:         "Sucessfully register",
			ReqBody:      validReq,
			StatusCode:   201,
			ExpectedResp: `{"message":"Registration successfully"}`,
		},
		{
			Name:         "Bad request password missing",
			ReqBody:      badReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"name, userId, password and emailId are mandatory to register"}`,
		},
		{
			Name:         "Bad request userId or emailId already registered",
			ReqBody:      duplicateReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"userId or emailId already registered"}`,
		},
		{
			Name:         "internal server error",
			ReqBody:      dbCrashReq,
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
		},
	}

	req := fasthttp.Request{}
	req.SetRequestURI("api/v1/register")
	req.Header.SetMethod("POST")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	mockDb := mock.MockDB{}
	registerCtx := handler.RegisterContext{
		LogonSvc: service.NewLogonService(&mockDb),
	}

	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		
		err := registerCtx.RegisterUser(c)
		assert.NoError(t, err, "Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), "expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), "response not matched")
	}
}

func Test_LoginUser(t *testing.T) {

	validReq := `{
		"userId": "validUser",
		"password":"abc2@100"
	}`
	badReq := `{
		"userId": "invalidUser",
		"password":"abc2@100"
	}`
	pwdNotProvidedReq := `{
		"userId": "invalidUser"
	}`
	wrongPwdReq := `{
		"userId": "validUser",
		"password":"XXX"
	}`
	dbCrashReq := `{
		"userId": "dbError",
		"password":"XXX"
	}`
	app := fiber.New()

	tests := []test{
		{
			Name:         "Sucessfully login",
			ReqBody:      validReq,
			StatusCode:   200,
			ExpectedResp: `{"message":"login successfull"}`,
		},
		{
			Name:         "Bad request password missing",
			ReqBody:      pwdNotProvidedReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"userId and password can not be empty or missing"}`,
		},
		{
			Name:         "password incorrect",
			ReqBody:      wrongPwdReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"incorrect password"}`,
		},
		{
			Name:         "user with given userId not present",
			ReqBody:      badReq,
			StatusCode:   400,
			ExpectedResp: `{"message":"User not found"}`,
		},
		{
			Name:         "internal server error",
			ReqBody:      dbCrashReq,
			StatusCode:   500,
			ExpectedResp: `{"message":"internal server error, please try after some time"}`,
		},
	}

	req := fasthttp.Request{}
	req.SetRequestURI("api/v1/login")
	req.Header.SetMethod("POST")
	req.Header.SetContentType(fiber.MIMEApplicationJSON)

	reqCtx := fasthttp.RequestCtx{
		Request: req,
	}
	c := app.AcquireCtx(&reqCtx)
	defer app.ReleaseCtx(c)
	mockDb := mock.MockDB{}
	registerCtx := handler.RegisterContext{
		LogonSvc: service.NewLogonService(&mockDb),
	}

	// run all test case
	for _, ts := range tests {
		reqCtx.Request.SetBody([]byte(ts.ReqBody))
		err := registerCtx.LoginUser(c)
		assert.NoError(t, err, ts.Name+" :Error is not expected")
		assert.Equal(t, ts.StatusCode, c.Response().StatusCode(), ts.Name+" :expected and actual status code different")
		assert.Equal(t, ts.ExpectedResp, string(c.Response().Body()), ts.Name+" :response not matched")
	}
}

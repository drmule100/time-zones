package handler

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2")

// IsAuthorize: authorizes with secret key
func IsAuthorize(c *fiber.Ctx) (*jwt.StandardClaims,error) {
	cookie := c.Cookies("token")

	token, err := jwt.ParseWithClaims(cookie, &jwt.StandardClaims{}, func(token *jwt.Token)(interface{}, error){
		return []byte(SecretKey), nil
	})
	if err != nil {
		return &jwt.StandardClaims{}, err
	}

	return token.Claims.(*jwt.StandardClaims), nil
}
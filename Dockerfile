FROM golang:latest as builder
COPY go.mod go.sum /go/src/gitlab.com/drmule100/time-zones/
WORKDIR /go/src/gitlab.com/drmule100/time-zones
RUN apt-get update && apt-get install -y ca-certificates openssl

ARG cert_location=/usr/local/share/ca-certificates

# Get certificate from "github.com"
RUN openssl s_client -showcerts -connect github.com:443 </dev/null 2>/dev/null|openssl x509 -outform PEM > ${cert_location}/github.crt
# Get certificate from "proxy.golang.org"
RUN openssl s_client -showcerts -connect proxy.golang.org:443 </dev/null 2>/dev/null|openssl x509 -outform PEM >  ${cert_location}/proxy.golang.crt
# Update certificates
RUN update-ca-certificates
RUN go mod download
COPY . /go/src/gitlab.com/drmule100/time-zones
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o build/time-zones gitlab.com/drmule100/time-zones

FROM alpine
COPY --from=builder /go/src/gitlab.com/drmule100/time-zones/build/time-zones /usr/bin/time-zones
EXPOSE 8080 8080
ENTRYPOINT ["/usr/bin/time-zones"]
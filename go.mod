module gitlab.com/drmule100/time-zones

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofiber/fiber/v2 v2.26.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	github.com/valyala/fasthttp v1.33.0
	go.mongodb.org/mongo-driver v1.8.3
	golang.org/x/crypto v0.0.0-20220131195533-30dcbda58838
)

package service

import (
	"context"
	"fmt"

	"gitlab.com/drmule100/time-zones/model"
)

type UserSvc struct {
	dbCtx model.DbHelper
}

func NewUserSvc(dbCtx model.DbHelper) *UserSvc {
	return &UserSvc{dbCtx: dbCtx}
}

func (svc *UserSvc) GetUser(ctx context.Context, userID string) (model.User, error) {
	user, err := svc.dbCtx.GetUserById(ctx, userID)
	if err != nil {
		return model.User{}, err
	}
	return user, err
}

func (svc *UserSvc) DeleteUser(ctx context.Context, userID string) error {
	// delete all timezones associated with user
	tzs, err := svc.dbCtx.GetTimezonesByUser(ctx, userID)
	if err != nil {
		fmt.Println("error on getting timezone for user:", err)
	} else {
		for _, tz := range tzs {
			err = svc.dbCtx.DeleteTimezone(ctx, tz.ID)
			if err != nil { //just log and continue
				fmt.Println("error on getting timezone for user:", err)
			}

		}
	}
	return svc.dbCtx.DeleteUser(ctx, userID)
}

func (svc *UserSvc) PromoteUser(ctx context.Context, userID string) error {
	user, err := svc.dbCtx.GetUserById(ctx, userID)
	if err != nil {
		return err
	}
	user.IsAdmin = true
	_, err = svc.dbCtx.UpdateUser(ctx, user)
	return err
}

func (svc *UserSvc) IsAdminUser(ctx context.Context, userID string) bool {
	user, err := svc.GetUser(ctx, userID)
	if err != nil {
		fmt.Printf("error while fetching user with id %s: %v", userID, err)
		return false
	}
	return user.IsAdmin
}

func (svc *UserSvc) GetAllUser(ctx context.Context) ([]model.User, error) {
	users, err := svc.dbCtx.GetAllUsers(ctx)
	if err != nil {
		fmt.Printf("error while fetching users with  %v", err)
		return []model.User{}, err
	}
	return users, nil
}

func (svc *UserSvc) UpdateUser(ctx context.Context, user model.User) (model.User, error) {
	user, err := svc.dbCtx.UpdateUser(ctx, user)
	if err != nil {
		fmt.Printf("error while updating user %s with  %v", user.UserID, err)
		return model.User{}, err
	}
	return user, nil
}

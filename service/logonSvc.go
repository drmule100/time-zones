package service

import (
	"context"
	"fmt"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/drmule100/time-zones/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type LogonSvc struct {
	dbCtx model.DbHelper
}

func NewLogonService(dbCtx model.DbHelper) *LogonSvc {
	return &LogonSvc{dbCtx: dbCtx}
}

func (svc *LogonSvc) SaveUser(ctx context.Context, user model.User) (int, error) {

	//checking if userId or EmailId already exist
	filterUserID := bson.D{primitive.E{Key: "userId", Value: user.UserID}}
	filterEmailID := bson.D{primitive.E{Key: "emailId", Value: user.EmailID}}

	orCondition := []bson.D{filterUserID, filterEmailID}
	filter := bson.D{primitive.E{Key: "$or", Value: orCondition}}

	_, err := svc.dbCtx.GetUserByFilter(ctx, filter)
	if err != mongo.ErrNoDocuments {
		return fiber.StatusBadRequest, fmt.Errorf("userId or emailId already registered")
	}
	return 0, svc.dbCtx.AddUser(ctx, user)
}

func (svc *LogonSvc) UserByID(ctx context.Context, id string) (model.User, error) {
	filter := bson.D{primitive.E{Key: "userId", Value: id}}
	return svc.dbCtx.GetUserByFilter(ctx, filter)
}

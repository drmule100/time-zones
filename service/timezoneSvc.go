package service

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/pkg/errors"
	"gitlab.com/drmule100/time-zones/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type TimezoneSvc struct {
	dbCtx   model.DbHelper
	userSvc *UserSvc
}

func New(dbCtx model.DbHelper) *TimezoneSvc {
	return &TimezoneSvc{dbCtx: dbCtx, userSvc: NewUserSvc(dbCtx)}
}

func (svc *TimezoneSvc) CreateTimeZone(ctx context.Context, tz *model.TimeZone) (*model.TimeZone, int, error) {

	// check if time zone is already present
	/* _, err := svc.dbCtx.GetTimezone(ctx, tz.ID) //todo check if same user has same timezone name
	if err != mongo.ErrNoDocuments {
		return nil, fiber.StatusBadRequest, fmt.Errorf("time zone with given name is already present, to update it use update/put api")
	} */

	res, err := svc.dbCtx.AddTimeZone(ctx, tz)
	if err != nil {
		return nil, fiber.StatusInternalServerError, err
	}
	return &res, fiber.StatusCreated, nil

}

func (svc *TimezoneSvc) GetTimeZones(ctx context.Context, userID string) ([]model.TimeZone, error) {
	if svc.userSvc.IsAdminUser(ctx, userID) {
		fmt.Println("user is admin fetching all records")
		return svc.dbCtx.GetAllTimezones(ctx)
	}
	return svc.dbCtx.GetTimezonesByUser(ctx, userID)
}

func (svc *TimezoneSvc) UpdateTimeZone(ctx context.Context, user, id string, tz model.TimeZone) (model.TimeZone, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return model.TimeZone{}, errors.Wrap(err, fmt.Sprintf("requested id %s is incorrect"))
	}
	oldTz, err := svc.dbCtx.GetTimezone(ctx, oid)
	if err != nil {
		return model.TimeZone{}, err
	}

	if oldTz.User != user && !svc.userSvc.IsAdminUser(ctx, user) {
		return model.TimeZone{}, fmt.Errorf("you are not allowed to modify %s timezone", id)
	}

	if tz.City == "" {
		tz.City = oldTz.City
	}
	if tz.DiffToGMT == "" {
		tz.DiffToGMT = oldTz.DiffToGMT
	}
	return svc.dbCtx.UpdateTimezone(ctx, oid, tz)
}

func (svc *TimezoneSvc) DeleteTimeZone(ctx context.Context, user, id string) (int, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fiber.StatusBadRequest, errors.Wrap(err, fmt.Sprintf("requested id %s is incorrect"))
	}
	tz, err := svc.dbCtx.GetTimezone(ctx, oid)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return fiber.StatusBadRequest, err
		}
		return fiber.StatusInternalServerError, fmt.Errorf("error while fetching timezone: %v", err)

	}

	if tz.User != user && !svc.userSvc.IsAdminUser(ctx, user) {
		return fiber.StatusUnauthorized, fmt.Errorf("you are not allowed to delete %s timezone", tz.ID)
	}
	return fiber.StatusInternalServerError, svc.dbCtx.DeleteTimezone(ctx, tz.ID)
}

func (svc *TimezoneSvc) GetTimezone(ctx context.Context, user, id string) (model.TimeZone, int, error) {
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return model.TimeZone{}, fiber.StatusBadRequest, errors.Wrap(err, fmt.Sprintf("requested id %s is incorrect"))
	}
	tz, err := svc.dbCtx.GetTimezone(ctx, oid)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return model.TimeZone{}, fiber.StatusBadRequest, err
		}
		return model.TimeZone{}, fiber.StatusInternalServerError, fmt.Errorf("error while fetching timezone: %v", err)
	}

	if tz.User != user && !svc.userSvc.IsAdminUser(ctx, user) {
		return model.TimeZone{}, fiber.StatusUnauthorized, fmt.Errorf("you are not allowed to fetch %s timezone", id)
	}
	return tz, fiber.StatusOK, nil
}

func (svc *TimezoneSvc) GetTime(ctx context.Context, user, id string) (time.Time, int, error) {

	tz, statusCode, err := svc.GetTimezone(ctx, user, id)
	if err != nil {
		return time.Time{}, statusCode, err
	}
	arr := strings.Split(tz.DiffToGMT, ":")
	if len(arr) < 2 && len(arr[0]) < 2 {
		return time.Time{}, fiber.StatusInternalServerError, fmt.Errorf("error while parsing time diff: %w", err)
	}
	offsetStr := ""
	if arr[0][0] == '-' {
		offsetStr = "-"
	} else {
		offsetStr = "+"
	}
	hr, err := strconv.Atoi(arr[0][1:])
	if err != nil {
		return time.Time{}, fiber.StatusInternalServerError, fmt.Errorf("error while parsing time diff: %w", err)
	}
	min, err := strconv.Atoi(arr[1])
	if err != nil {
		return time.Time{}, fiber.StatusInternalServerError, fmt.Errorf("error while parsing time diff: %w", err)
	}
	offsetMin := (60 * +hr) + min
	gmtDiff := fmt.Sprintf("%s%d%s", offsetStr, offsetMin, "m")
	offSet, err := time.ParseDuration(gmtDiff)
	if err != nil {
		return time.Time{}, statusCode, err
	}
	t := time.Now().UTC().Add(offSet) // utc and gmt are same
	return t, statusCode, err
}

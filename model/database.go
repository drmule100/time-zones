package model

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type DbContext struct {
	dbClient *mongo.Client
}

type DbHelper interface {
	//timezone action
	GetTimezone(ctx context.Context, id primitive.ObjectID) (TimeZone, error)
	UpdateTimezone(ctx context.Context, id primitive.ObjectID, tz TimeZone) (TimeZone, error)
	DeleteTimezone(ctx context.Context, id primitive.ObjectID) error
	GetTimezonesByUser(ctx context.Context, userId string) ([]TimeZone, error)
	AddTimeZone(ctx context.Context, tz *TimeZone) (TimeZone, error)
	GetAllTimezones(ctx context.Context) ([]TimeZone, error)

	//users action
	GetUserByFilter(ctx context.Context, filter bson.D) (User, error)
	GetUserById(ctx context.Context, id string) (User, error)
	AddUser(ctx context.Context, user User) error
	GetAllUsers(ctx context.Context) ([]User, error)
	UpdateUser(ctx context.Context, user User) (User, error)
	DeleteUser(ctx context.Context, userId string) error
}

func New(ctx context.Context) (*DbContext, error) {
	client, err := connect(ctx)
	if err != nil {
		return nil, fmt.Errorf("error while connecting to database: %w", err)
	}
	return &DbContext{dbClient: client}, nil
}

func connect(ctx context.Context) (*mongo.Client, error) {
	url := /* "mongodb://localhost:27017" // */"mongodb+srv://drmule100:drmule100@sandbox.bjtfy.mongodb.net/sample_airbnb?retryWrites=true&w=majority"
	clientOptions := options.Client().
		ApplyURI(url)

	client, err := mongo.Connect(ctx, clientOptions)
	if err != nil {
		return &mongo.Client{}, fmt.Errorf("error while connecting db: %v", err)
	}
	return client, nil
}

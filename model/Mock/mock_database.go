package mock

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/drmule100/time-zones/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	InternalServerError      = "internal server error, please try after some time"
	InternalServerErrorShort = "internal server error"
)

type MockDB struct {
}

//timezone action
func (db *MockDB) GetTimezone(ctx context.Context, id primitive.ObjectID) (model.TimeZone, error) {
	key := ctx.Value("Name")
	keyStr, _ := key.(string)
	if strings.Contains(keyStr, "new") || id.String() == "ObjectID(\"000000000000000000000002\")" {
		return model.TimeZone{}, mongo.ErrNoDocuments
	}
	if strings.Contains(keyStr, "dbError") || id.String() == "ObjectID(\"000000000000000000000003\")" {
		return model.TimeZone{}, fmt.Errorf(InternalServerError)
	}
	if key == "success" || id.String() == "ObjectID(\"000000000000000000000001\")" {
		return model.TimeZone{
			Name:      "IST1",
			User:      "user2",
			DiffToGMT: "-6:00",
		}, nil
	}
	if key == "abc1-IST" {
		return model.TimeZone{
			Name:      "IST",
			User:      "abc1",
			DiffToGMT: "-6:00",
		}, nil
	}
	return model.TimeZone{}, nil
}
func (db *MockDB) UpdateTimezone(ctx context.Context, id primitive.ObjectID, tz model.TimeZone) (model.TimeZone, error) {
	key := ctx.Value("Name")
	keyStr, _ := key.(string)
	if tz.City == "dbError" {
		return model.TimeZone{}, fmt.Errorf(InternalServerError)
	}
	if keyStr == "success" {
		return model.TimeZone{
			Name:      "IST1",
			User:      "user2",
			DiffToGMT: "-6:00",
		}, nil
	}
	return model.TimeZone{}, nil
}
func (db *MockDB) DeleteTimezone(ctx context.Context, id primitive.ObjectID) error {
	key := "" //todo fixME
	if key == "user2-DbError" {
		return fmt.Errorf(InternalServerErrorShort)
	}
	return nil
}
func (db *MockDB) GetTimezonesByUser(ctx context.Context, userId string) ([]model.TimeZone, error) {

	if strings.Contains(userId, "new") {
		return []model.TimeZone{}, mongo.ErrNoDocuments
	}
	if strings.Contains(userId, "DbError") {
		return []model.TimeZone{}, fmt.Errorf(InternalServerError)
	}
	return []model.TimeZone{
		{
			Name:      "IST1",
			User:      "user2",
			DiffToGMT: "-6:00",
		},
		{
			Name:      "IST2",
			User:      "user2",
			DiffToGMT: "-6:30",
		},
	}, nil
}

func (db *MockDB) AddTimeZone(ctx context.Context, tz *model.TimeZone) (model.TimeZone, error) {

	if tz.City == "dbError" {
		return *tz, fmt.Errorf(InternalServerError)
	}
	return *tz, nil
}
func (db *MockDB) GetAllTimezones(ctx context.Context) ([]model.TimeZone, error) {
	return []model.TimeZone{
		{
			Name:      "IST1",
			User:      "user2",
			DiffToGMT: "-6:00",
		},
		{
			Name:      "IST2",
			User:      "user2",
			DiffToGMT: "-6:30",
		},
	}, nil
}

//users action
func (db *MockDB) GetUserByFilter(ctx context.Context, filter bson.D) (model.User, error) {

	fm, ok := filter.Map()["$or"]
	if ok {
		qd := fm.([]bson.D)
		userId := qd[0][0].Value
		if userId.(string) != "xxx_Already_Exist_XXX" {
			return model.User{}, mongo.ErrNoDocuments
		}
	} else {
		userId := filter[0].Value
		if userId.(string) == "validUser" { // success login case
			//pwd, _ := bcrypt.GenerateFromPassword([]byte("user2@100"), 14)
			return model.User{
				Name:            "validUser",
				CryptedPassword: []uint8("$2a$14$QQGRwj5gP2R2x.TmifxIp.DgA8iHBq4AIVktUhMWnaAbMnt1SGuBK"),
			}, nil
		}
		if userId.(string) == "dbError" { //internal server error
			return model.User{}, fmt.Errorf(InternalServerErrorShort)
		}
		if userId.(string) == "invalidUser" { // no user found login case
			return model.User{}, mongo.ErrNoDocuments
		}
	}

	return model.User{}, nil
}
func (db *MockDB) GetUserById(ctx context.Context, id string) (model.User, error) {
	if id == "user2" {
		return model.User{
			Name:    "User2",
			UserID:  "user2",
			EmailID: "user2@email.com",
			//IsAdmin: true,
		}, nil
	}

	if id == "admin" {
		return model.User{
			Name:    "admin",
			UserID:  "admin",
			EmailID: "admin@email.com",
			IsAdmin: true,
		}, nil
	}

	if id == "user1" {
		return model.User{
			Name:    "User1",
			UserID:  "user1",
			EmailID: "user1@email.com",
			IsAdmin: false,
		}, nil
	}
	if id == "notUser" {
		return model.User{}, mongo.ErrNoDocuments
	}
	if id == "user2-DbError" || id == "userError" {
		return model.User{}, fmt.Errorf(InternalServerError)
	}
	return model.User{}, nil
}
func (db *MockDB) AddUser(ctx context.Context, user model.User) error {
	if user.UserID == "dbError" {
		return fmt.Errorf(InternalServerError)
	}
	return nil
}
func (db *MockDB) GetAllUsers(ctx context.Context) ([]model.User, error) {

	return []model.User{
		{
			Name:    "User2",
			UserID:  "user2",
			EmailID: "user2@email.com",
		},
	}, nil
}
func (db *MockDB) UpdateUser(ctx context.Context, user model.User) (model.User, error) {
	if user.Name == "badUser" {
		return model.User{}, fmt.Errorf(InternalServerError)
	}

	return user, nil
}
func (db *MockDB) DeleteUser(ctx context.Context, userId string) error {
	if userId == "userError" {
		return fmt.Errorf(InternalServerError)
	}
	if userId == "notUser" {
		return mongo.ErrNoDocuments
	}
	return nil
}

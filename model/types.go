package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type User struct {
	Name            string `json:"name" bson:"name"`
	UserID          string `json:"userId" bson:"userId"`
	EmailID         string `json:"emailId" bson:"emailId"`
	Password        string `json:"password,omitempty" bson:"-"`
	CryptedPassword []byte `json:"-" bson:"password"`
	IsAdmin         bool   `json:"-" bson:"admin"`
}

type LoginCredientials struct {
	UserID   string `json:"userId"`
	Password string `json:"password"`
}

type TimeZone struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	User      string             `json:"-" bson:"user"`
	Name      string             `json:"name" bson:"name"`
	City      string             `json:"city" bson:"city"`
	DiffToGMT string             `json:"GMT_Difference" bson:"GMT_Difference"`
}

package model

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//GetUserByFilter fetch one record from db which matches filter criteria
func (dbCtx *DbContext) GetUserByFilter(ctx context.Context, filter bson.D) (User, error) {

	collection := dbCtx.dbClient.Database(userDB).Collection(userCollection)

	resp := collection.FindOne(ctx, filter, &options.FindOneOptions{})
	if resp.Err() != nil {
		return User{}, resp.Err()
	}

	var user User
	err := resp.Decode(&user)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func (dbCtx *DbContext) GetUserById(ctx context.Context, id string) (User, error) {
	filterUserID := bson.D{primitive.E{Key: "userId", Value: id}}
	return dbCtx.GetUserByFilter(ctx, filterUserID)
}

func (dbCtx *DbContext) AddUser(ctx context.Context, user User) error {
	collection := dbCtx.dbClient.Database(userDB).Collection(userCollection)
	_, err := collection.InsertOne(ctx, &user, nil)
	if err != nil {
		return fmt.Errorf("collection.InsertOne() failed %v", err)
	}
	return nil
}

//GetAllUsers fetches all users present in db
func (dbCtx *DbContext) GetAllUsers(ctx context.Context) ([]User, error) {

	collection := dbCtx.dbClient.Database(userDB).Collection(userCollection)

	cur, err := collection.Find(ctx, bson.D{}, &options.FindOptions{})
	if err != nil {
		return []User{}, err
	}
	var users []User
	err = cur.All(ctx, &users)
	if err != nil {
		return []User{}, err
	}
	return users, nil
}

func (dbCtx *DbContext) UpdateUser(ctx context.Context, user User) (User, error) {

	coll := dbCtx.dbClient.Database(userDB).Collection(userCollection)
	filter := bson.D{primitive.E{Key: "userId", Value: user.UserID}}
	update := bson.M{
		"$set": user,
	}
	_, err := coll.UpdateOne(ctx, filter, update, &options.UpdateOptions{})
	if err != nil {
		return User{}, err
	}
	return user, nil
}

// deletes user from DB
func (dbCtx *DbContext) DeleteUser(ctx context.Context, userId string) error {

	filter := bson.D{primitive.E{Key: "userId", Value: userId}}
	collection := dbCtx.dbClient.Database(userDB).Collection(userCollection)

	_, err := collection.DeleteOne(ctx, filter, &options.DeleteOptions{})
	if err != nil {
		return err
	}

	return nil
}

package model

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	userDB         = "TimeZone"
	collection     = "timezone"
	userCollection = "user"
)

func (dbCtx *DbContext) GetTimezone(ctx context.Context, id primitive.ObjectID) (TimeZone, error) {

	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")
	//filter := bson.D{primitive.E{Key: "_id", Value: id}}
	tzResp := coll.FindOne(ctx, bson.M{"_id": id}, &options.FindOneOptions{})
	if tzResp.Err() != nil {
		return TimeZone{}, tzResp.Err()
	}

	var tz TimeZone
	err := tzResp.Decode(&tz)
	if err != nil {
		return TimeZone{}, err
	}
	return tz, nil
}

func (dbCtx *DbContext) UpdateTimezone(ctx context.Context, id primitive.ObjectID, tz TimeZone) (TimeZone, error) {

	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")
	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	update := bson.M{
		"$set": tz,
	}
	_, err := coll.UpdateOne(ctx, filter, update, &options.UpdateOptions{})
	if err != nil {
		return TimeZone{}, err
	}
	return dbCtx.GetTimezone(ctx, id)
}

func (dbCtx *DbContext) DeleteTimezone(ctx context.Context, id primitive.ObjectID) error {

	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")
	filter := bson.D{primitive.E{Key: "_id", Value: id}}
	_, err := coll.DeleteOne(ctx, filter, &options.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}

func (dbCtx *DbContext) GetTimezonesByUser(ctx context.Context, userId string) ([]TimeZone, error) {
	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")
	filter := bson.D{primitive.E{Key: "user", Value: userId}}
	cur, err := coll.Find(ctx, filter, &options.FindOptions{})
	if err != nil {
		return []TimeZone{}, err
	}
	var tzs []TimeZone
	err = cur.All(ctx, &tzs)
	if err != nil {
		return []TimeZone{}, err
	}
	return tzs, nil
}

func (dbCtx *DbContext) AddTimeZone(ctx context.Context, tz *TimeZone) (TimeZone, error) {
	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")

	res, err := coll.InsertOne(ctx, tz, options.InsertOne())
	if err != nil {
		fmt.Printf("collection.InsertOne() failed: %v", err)
		return *tz, err
	}
	return dbCtx.GetTimezone(ctx, res.InsertedID.(primitive.ObjectID))
}

func (dbCtx *DbContext) GetAllTimezones(ctx context.Context) ([]TimeZone, error) {
	coll := dbCtx.dbClient.Database(userDB).Collection("timezone")
	filter := bson.D{} //get all time zones
	cur, err := coll.Find(ctx, filter, &options.FindOptions{})
	if err != nil {
		return []TimeZone{}, err
	}
	var tzs []TimeZone
	err = cur.All(ctx, &tzs)
	if err != nil {
		return []TimeZone{}, err
	}
	return tzs, nil

}
